package br.com.sementex.trend.trend.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.loopj.android.image.SmartImageView;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import br.com.sementex.trend.trend.R;
import br.com.sementex.trend.trend.model.Restaurante;

public class BackstageRecyclerAdapter extends RecyclerView.Adapter<BackstageRecyclerAdapter.MyViewHolder> {

    private ArrayList<Restaurante> al_obj;
    private ArrayList<Restaurante> al_obj_orig;

    private Context context;
    private Activity activity;
    private AdapterCallback callBack;

    private SmartImageView iv_ImagemPost;
    private TextView tv_nomeEst;
    private TextView tv_DescricaoEst;

    public BackstageRecyclerAdapter(Activity activity, AdapterCallback callback, Context context, ArrayList<Restaurante> al_obj) {
        this.context = context;
        this.activity = activity;
        this.al_obj_orig = al_obj_orig;
        this.al_obj = al_obj;
        this.callBack = callback;

    }

    @Override
    public BackstageRecyclerAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_backstage, parent, false);
        final MyViewHolder holder = new MyViewHolder(view);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callBack.onbackstageSelecionadoResult(holder.est, holder.getAdapterPosition());
            }
        });

        return holder;
        //return new BackstageRecyclerAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
         final Restaurante est = al_obj.get(holder.getAdapterPosition());

        //iv_ImagemPost.setImageDrawable(LoadImageFromWebOperations(est.getImagem(), 1));
        tv_nomeEst.setText(est.getNome());
        tv_DescricaoEst.setText(est.getDescricao());
        iv_ImagemPost.setImageUrl(est.getImagem());
    }

    @Override
    public int getItemCount() {
        return al_obj.size();
    }

    //VIEW HOLDER - INNER CLASS
    public class MyViewHolder extends RecyclerView.ViewHolder {


        private Restaurante est;

        MyViewHolder(View itemView) {
            super(itemView);
            tv_nomeEst = itemView.findViewById(R.id.tv_nomeEst);
            tv_DescricaoEst = itemView.findViewById(R.id.tv_DescricaoEst);
            iv_ImagemPost = itemView.findViewById(R.id.iv_ImagemPost);
        }
    }

    public interface AdapterCallback {
        void onbackstageSelecionadoResult(Restaurante c, int position);
    }

    public Bitmap writeText(Bitmap bitmap, int x, int y, String text) {
        Canvas canvas = new Canvas(bitmap);
        Paint paint = new Paint();
        paint.setColor(Color.WHITE);
        paint.setTextSize(200);
        canvas.drawText(text, x, y, paint);
        return bitmap;
    }

    //public static Drawable LoadImageFromWebOperations(String url, int count) {
    //    try {
    //        InputStream is = (InputStream) new URL(url).getContent();
    //        Drawable d = Drawable.createFromStream(is, "srcName" + count);
    //        return d;
    //    } catch (Exception e) {
    //        return null;
    //    }
    //}

}
