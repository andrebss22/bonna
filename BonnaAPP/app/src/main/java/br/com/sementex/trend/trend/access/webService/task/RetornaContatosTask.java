package br.com.sementex.trend.trend.access.webService.task;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;

import br.com.sementex.trend.trend.access.webService.WSMethodCalls;
import br.com.sementex.trend.trend.model.Contato;
import br.com.sementex.trend.trend.model.Endereco;
import br.com.sementex.trend.trend.model.Restaurante;

import static br.com.sementex.trend.trend.access.model.WebService.NAMESPACE;
import static br.com.sementex.trend.trend.access.model.WebService.SOAP_ADRESS;
import static br.com.sementex.trend.trend.access.model.WebService.TIMEOUT;
import static br.com.sementex.trend.trend.access.model.WebService.URL_SOAP;

/**
 * Created by Renan Azalim on 22/06/2018.
 */

public class RetornaContatosTask extends AsyncTask<String, String, ArrayList<Contato>> {
    boolean erro = false;
    String tipoErro = null;
    Context cont;
    private WSMethodCalls ws;

    public RetornaContatosTask(Context cont) {
        this.cont = cont;
    }

    @Override
    protected void onProgressUpdate(String... values) {
    }

    @Override
    protected ArrayList<Contato> doInBackground(String... params) {
        Log.i("TASK I  Retorna Modulos", "Executanto Task");
        ArrayList<Contato> retorno = new ArrayList<>();

        try {
            retorno = RetornaContatos();
        } catch (Exception e) {
            Log.i("TASK I Restaurantes", "Exception - Erro na execução da Task");
            if(e.toString().contains("UnknownHostException") || e.toString().contains("EOFException") || e.toString().contains("SocketTimeoutException")){
                tipoErro = "Não foi possível conectar com o servidor!";
                retorno = null;
            }
            erro = true;
            Log.e(this.getClass().getSimpleName(),e.getMessage(), e);
        }
        return retorno;
    }

    public ArrayList<Contato> RetornaContatos() throws IOException, XmlPullParserException {
        ws = new WSMethodCalls(cont);
        final String METHOD_NAME = "RetornaContatos";
        final String SOAP_ACTION = SOAP_ADRESS + METHOD_NAME;

        ArrayList<Contato> lista = new ArrayList<>();
        Log.i("Serviço", METHOD_NAME + " - Inicializando serviço ...");

        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
        //request.addProperty("IdSmtUsuario", IdSmtUsuario);
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);
        envelope.addMapping(NAMESPACE, METHOD_NAME, new Contato().getClass());
        SoapObject response;
        ArrayList<HeaderProperty> headerPropertyArrayList = new ArrayList<>();
        headerPropertyArrayList.add(new HeaderProperty("Connection", "close"));
        HttpTransportSE androidHttpTransport = new HttpTransportSE(URL_SOAP, TIMEOUT);
        //androidHttpTransport.debug = true;
        androidHttpTransport.call(SOAP_ACTION, envelope);
        //System.out.println(androidHttpTransport.requestDump);
        //System.out.println(androidHttpTransport.responseDump);
        response = (SoapObject) envelope.getResponse();


        if (response != null) {
            if(response.getPropertyCount() > 0) {
                for (int i = 0; i < response.getPropertyCount(); i++) {
                    SoapObject us = (SoapObject) response.getProperty(i);
                    Contato cont = new Contato();


                    cont.setDescricao(us.getProperty("Descricao").toString());
                    cont.setFixoSN(Boolean.parseBoolean(us.getProperty("FixoSN").toString()));
                    cont.setIDContato(Integer.parseInt(us.getProperty("IDContato").toString()));
                    cont.setIDRestaurante(Integer.parseInt(us.getProperty("IDRestaurante").toString()));
                    cont.setNumero(us.getProperty("Numero").toString());
                    lista.add(cont);
                }
            }
        } else if (response == null) {
            Log.i("Serviço", METHOD_NAME + " - FIM - Lista não Preenchida");
            lista = null;
        }
        return lista;
    }
}
