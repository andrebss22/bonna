package br.com.sementex.trend.trend.access.webService.task;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;

import br.com.sementex.trend.trend.access.webService.WSMethodCalls;
import br.com.sementex.trend.trend.model.Restaurante;

import static br.com.sementex.trend.trend.access.model.WebService.NAMESPACE;
import static br.com.sementex.trend.trend.access.model.WebService.SOAP_ADRESS;
import static br.com.sementex.trend.trend.access.model.WebService.TIMEOUT;
import static br.com.sementex.trend.trend.access.model.WebService.URL_SOAP;


public class RetornaRestaurantesTask extends AsyncTask<String, String, ArrayList<Restaurante>> {
    boolean erro = false;
    String tipoErro = null;
    Context cont;
    private WSMethodCalls ws;

    public RetornaRestaurantesTask(Context cont) {
        this.cont = cont;
    }


    @Override
    protected void onProgressUpdate(String... values) {
    }

    @Override
    protected ArrayList<Restaurante> doInBackground(String... params) {
        Log.i("TASK I  Retorna Modulos", "Executanto Task");
        ArrayList<Restaurante> retorno = new ArrayList<>();

        try {
            retorno = RetornaRestaurantes();
        } catch (Exception e) {
            Log.i("TASK I Restaurantes", "Exception - Erro na execução da Task");
            if(e.toString().contains("UnknownHostException") || e.toString().contains("EOFException") || e.toString().contains("SocketTimeoutException")){
                tipoErro = "Não foi possível conectar com o servidor!";
                retorno = null;
            }
            erro = true;
            Log.e(this.getClass().getSimpleName(),e.getMessage(), e);
        }
        return retorno;
    }


    public ArrayList<Restaurante> RetornaRestaurantes() throws IOException, XmlPullParserException {
        ws = new WSMethodCalls(cont);
        final String METHOD_NAME = "RetornaRestaurantes";
        final String SOAP_ACTION = SOAP_ADRESS + METHOD_NAME;

        ArrayList<Restaurante> lista = new ArrayList<>();
        Log.i("Serviço", METHOD_NAME + " - Inicializando serviço ...");

        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
        //request.addProperty("IdSmtUsuario", IdSmtUsuario);
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);
        envelope.addMapping(NAMESPACE, METHOD_NAME, new Restaurante().getClass());
        SoapObject response;
        ArrayList<HeaderProperty> headerPropertyArrayList = new ArrayList<>();
        headerPropertyArrayList.add(new HeaderProperty("Connection", "close"));
        HttpTransportSE androidHttpTransport = new HttpTransportSE(URL_SOAP, TIMEOUT);
        //androidHttpTransport.debug = true;
        androidHttpTransport.call(SOAP_ACTION, envelope);
        //System.out.println(androidHttpTransport.requestDump);
        //System.out.println(androidHttpTransport.responseDump);
        response = (SoapObject) envelope.getResponse();


        if (response != null) {
            if(response.getPropertyCount() > 0) {
                for (int i = 0; i < response.getPropertyCount(); i++) {
                    SoapObject us = (SoapObject) response.getProperty(i);
                    Restaurante rest = new Restaurante();

                    rest.setIDRestaurante(Integer.parseInt(us.getProperty("IDRestaurante").toString()));
                    rest.setAtivoSN(Boolean.parseBoolean(us.getProperty("AtivoSN").toString()));
                    rest.setCliques(Integer.parseInt(us.getProperty("Cliques").toString()));
                    rest.setDescricao(us.getProperty("Descricao").toString());
                    rest.setImagem(us.getProperty("Imagem").toString());
                    rest.setNome(us.getProperty("Nome").toString());
                    rest.setPremiumSN(Boolean.parseBoolean(us.getProperty("PremiumSN").toString()));
                    rest.setRotacaoSN(Boolean.parseBoolean(us.getProperty("RotacaoSN").toString()));
                    lista.add(rest);
                }
            }
        } else if (response == null) {
            Log.i("Serviço", METHOD_NAME + " - FIM - Lista não Preenchida");
            lista = null;
        }
        return lista;
    }


}
