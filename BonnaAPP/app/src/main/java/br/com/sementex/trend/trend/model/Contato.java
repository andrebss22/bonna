package br.com.sementex.trend.trend.model;

public class Contato {

    public Contato(){
    }

    private int IDContato;
    private int IDRestaurante;
    private boolean FixoSN;
    private String Descricao;
    private String Numero;

    public int getIDContato() {
        return IDContato;
    }

    public void setIDContato(int IDContato) {
        this.IDContato = IDContato;
    }

    public int getIDRestaurante() {
        return IDRestaurante;
    }

    public void setIDRestaurante(int IDRestaurante) {
        this.IDRestaurante = IDRestaurante;
    }

    public boolean isFixoSN() {
        return FixoSN;
    }

    public void setFixoSN(boolean fixoSN) {
        FixoSN = fixoSN;
    }

    public String getDescricao() {
        return Descricao;
    }

    public void setDescricao(String descricao) {
        Descricao = descricao;
    }

    public String getNumero() {
        return Numero;
    }

    public void setNumero(String numero) {
        Numero = numero;
    }
}
