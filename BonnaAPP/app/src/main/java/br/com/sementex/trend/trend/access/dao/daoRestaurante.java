package br.com.sementex.trend.trend.access.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.util.Log;

import java.util.ArrayList;

import br.com.sementex.trend.trend.access.database.DB;
import br.com.sementex.trend.trend.access.database.DBStrings;
import br.com.sementex.trend.trend.model.Restaurante;
import br.com.sementex.trend.trend.util.formatters.DateFormatter;

public class daoRestaurante {
    private DB conexao;

    public daoRestaurante(Context context) {
        this.conexao = DB.getInstance(context);
    }

    public boolean adicionarRestaurantes(Restaurante r) {
        long insertOk;
        boolean result = true;
        try {
            conexao.getWritableDatabase().beginTransaction();
            ContentValues values = new ContentValues();
             values.put(DBStrings.IDRestauranteTbRest, r.getIDRestaurante());
             values.put(DBStrings.ImagemTbRest, r.getImagem());
             values.put(DBStrings.NomeTbRest, r.getNome());
             values.put(DBStrings.DescricaoTbRest, r.getDescricao());
             values.put(DBStrings.AtivoSNTbRest, r.isAtivoSN());
             values.put(DBStrings.PremiumSNTbRest, r.isPremiumSN());
             values.put(DBStrings.RotacaoSNTbRest, r.isRotacaoSN());
             values.put(DBStrings.CliquesTbRest, r.getCliques());
             values.put(DBStrings.DataUploadTbRest, DateFormatter.currentDate().toString());

             try {
                 insertOk = conexao.getWritableDatabase().insert(DBStrings.TbRest, null, values);
                 if (insertOk == -1) {
                     result = false;
                 }
             } catch (Exception e) {
                 Log.e(this.getClass().getSimpleName(), e.getMessage(), e);
                 result = false;
             }
             conexao.getWritableDatabase().setTransactionSuccessful();

            return result;
        } catch (SQLException e) {
            Log.e("DBStrings", e.getMessage(), e);
            return false;
        } finally {
            conexao.getWritableDatabase().endTransaction();
            conexao.close();
        }
    }

    public ArrayList<Restaurante> retornaRestaurantes() {
        String query = "SELECT IDRestaurante, Imagem, Nome, Descricao, Cliques, RotacaoSN, PremiumSN, AtivoSN FROM TbRest";
        Cursor c = conexao.getWritableDatabase().rawQuery(query, null);

        ArrayList<Restaurante> lista = new ArrayList<>();
        c.moveToFirst();

        while (!c.isAfterLast()) {
            Restaurante r = new Restaurante();

            r.setIDRestaurante(c.getInt(0));
            r.setImagem(c.getString(1));
            r.setNome(c.getString(2));
            r.setDescricao(c.getString(3));
            r.setCliques(c.getInt(4));
            r.setRotacaoSN(c.getInt(5)==1);
            r.setPremiumSN(c.getInt(6)==1);
            r.setAtivoSN(c.getInt(7)==1);

            lista.add(r);
            c.moveToNext();
        }
        c.close();
        return lista;
    }

    public void apagarRestaurantes(ArrayList<Restaurante> restList){
        for(Restaurante r : restList){
            conexao.getWritableDatabase().delete(DBStrings.TbRest, DBStrings.IDRestauranteTbRest + " = " + r.getIDRestaurante() , null);
        }

        conexao.close();
    }

}
