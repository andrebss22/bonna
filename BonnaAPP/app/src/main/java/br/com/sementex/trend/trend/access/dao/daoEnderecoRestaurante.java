package br.com.sementex.trend.trend.access.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.SQLException;
import android.util.Log;
import br.com.sementex.trend.trend.access.database.DB;
import br.com.sementex.trend.trend.access.database.DBStrings;
import br.com.sementex.trend.trend.model.Endereco;

public class daoEnderecoRestaurante {
    private DB conexao;

    public daoEnderecoRestaurante(Context context) {
        this.conexao = DB.getInstance(context);
    }

    public boolean adicionarEnderecosRestaurante(Endereco end) {
        long insertOk;
        boolean result = true;
        try {
            conexao.getWritableDatabase().beginTransaction();
            ContentValues values = new ContentValues();
            values.put(DBStrings.IDEnderecoTbEnderRest, end.getIDEndereco());
            values.put(DBStrings.BairroTbEnderRest, end.getBairro());
            values.put(DBStrings.CEPTbEnderRest, end.getCEP());
            values.put(DBStrings.EstadoTbEnderRest, end.getEstado());
            values.put(DBStrings.CidadeTbEnderRest, end.getCidade());
            values.put(DBStrings.DescricaoTbEnderRest, end.getDescricao());
            values.put(DBStrings.IDRestauranteTbEnderRest, end.getIDRestaurante());
            values.put(DBStrings.LogradouroTbEnderRest, end.getLogradouro());
            values.put(DBStrings.MatrizSNTbEnderRest, end.isMatrizSN());
            values.put(DBStrings.NumeroTbEnderRest, end.getNumero());

            try {
                insertOk = conexao.getWritableDatabase().insert(DBStrings.TbEnderRest, null, values);
                if (insertOk == -1) {
                    result = false;
                }
            } catch (Exception e) {
                Log.e(this.getClass().getSimpleName(), e.getMessage(), e);
                result = false;
            }

            conexao.getWritableDatabase().setTransactionSuccessful();

            return result;
        } catch (SQLException e) {
            Log.e("DBStrings", e.getMessage(), e);
            return false;
        } finally {
            conexao.getWritableDatabase().endTransaction();
            conexao.close();
        }
    }
}
