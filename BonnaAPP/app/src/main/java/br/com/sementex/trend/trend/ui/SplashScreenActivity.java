package br.com.sementex.trend.trend.ui;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.sementex.trend.trend.R;
import br.com.sementex.trend.trend.access.dao.daoContato;
import br.com.sementex.trend.trend.access.dao.daoEnderecoRestaurante;
import br.com.sementex.trend.trend.access.dao.daoRestaurante;
import br.com.sementex.trend.trend.access.webService.WSMethodCalls;
import br.com.sementex.trend.trend.model.Contato;
import br.com.sementex.trend.trend.model.Endereco;
import br.com.sementex.trend.trend.model.Restaurante;


public class SplashScreenActivity extends AppCompatActivity {
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    public static final String TAG = SplashScreenActivity.class.getSimpleName();
    private int int_atraso = 4000;
    private WSMethodCalls ws;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        if(verificarPedirPermissoes()) {
            if(carregarDados()){
                iniciarAplicativo();
            }

        }
    }

    private  boolean verificarPedirPermissoes() {
        int permissaoIMEI = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);
        int permissaoGPS = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        int readExternalStorage = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        int writeExternalStorage = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        List<String> lstPermissoesNecessaria = new ArrayList<>();
        if (permissaoGPS != PackageManager.PERMISSION_GRANTED) {
            lstPermissoesNecessaria.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }
        if (permissaoIMEI != PackageManager.PERMISSION_GRANTED) {
            lstPermissoesNecessaria.add(Manifest.permission.READ_PHONE_STATE);
        }
        if (readExternalStorage != PackageManager.PERMISSION_GRANTED) {
            lstPermissoesNecessaria.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (writeExternalStorage != PackageManager.PERMISSION_GRANTED) {
            lstPermissoesNecessaria.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (!lstPermissoesNecessaria.isEmpty()) {
            ActivityCompat.requestPermissions(this, lstPermissoesNecessaria.toArray(new String[lstPermissoesNecessaria.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {

        switch (requestCode) {
            case REQUEST_ID_MULTIPLE_PERMISSIONS: {
                Map<String, Integer> perms = new HashMap<>();
                // Inicia map com as duas permissoes
                perms.put(Manifest.permission.READ_PHONE_STATE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.ACCESS_FINE_LOCATION, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                // Preenche resultados com status atual
                if (grantResults.length > 0) {
                    for (int i = 0; i < permissions.length; i++)
                        perms.put(permissions[i], grantResults[i]);
                    // Verifica as permissoes
                    if (perms.get(Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED
                            && perms.get(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                            && perms.get(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                            && perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        Log.d(TAG, "Permissões garantidas");
                        // Executa fluxo normal
                        iniciarAplicativo();
                        //else qdo uma ou todas as permissões são negadas
                    } else {
                        Log.d(TAG, "Some permissions are not granted ask again ");
                        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_PHONE_STATE)
                                || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)
                                || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                                || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                            showDialogOK(new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            switch (which) {
                                                case DialogInterface.BUTTON_POSITIVE:
                                                    verificarPedirPermissoes();
                                                    break;
                                                case DialogInterface.BUTTON_NEGATIVE:
                                                    finish();
                                                    break;
                                            }
                                        }
                                    });
                        }
                        else {
                            Toast.makeText(this, "Vá nas Configurações do telefone a altere as Permissões", Toast.LENGTH_LONG).show();
                            //encerra o aplicativo
                            finish();
                        }
                    }
                }
            }
        }

    }

    private void showDialogOK(DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(this)
                .setMessage("Permissões para acessar o local do dispositivo e gerenciar chamadas são necessárias para o funcionamento do aplicativo")
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancelar", okListener)
                .create()
                .show();
    }

    private void iniciarAplicativo(){
        Thread thr_contador = new Thread() {
            @Override
            public void run() {
                try {
                    synchronized (this) {
                        wait(int_atraso);
                    }
                } catch (InterruptedException e) {
                    Log.v(this.getClass().getSimpleName(), e.getMessage(), e);
                    Thread.currentThread().interrupt();
                }
                startActivity(new Intent(SplashScreenActivity.this, MainActivity.class));
                finish();
            }
        };
        thr_contador.start();
    }

    private boolean carregarDados(){
        boolean result = false;
        ws = new WSMethodCalls(getApplicationContext());

        ArrayList<Restaurante> rest = new ArrayList<>();
        ArrayList<Contato> cont = new ArrayList<>();
        ArrayList<Endereco> endRest = new ArrayList<>();

        daoRestaurante daoRest = new daoRestaurante(getApplicationContext());
        daoContato daoCont = new daoContato(getApplicationContext());
        daoEnderecoRestaurante daoEndRest = new daoEnderecoRestaurante(getApplicationContext());

        rest = ws.RetornaRestaurantesTask();
        cont = ws.RetornaContatosTask();
        endRest = ws.RetornaEnderecosTask();

        for(Restaurante r : rest){
            daoRest.adicionarRestaurantes(r);
        }
        for(Contato c : cont){
            daoCont.adicionarContatos(c);
        }
        for(Endereco e : endRest){
            daoEndRest.adicionarEnderecosRestaurante(e);
        }

        if(rest.size() > 0 && cont.size() > 0 && endRest.size() > 0){
            result = true;
        }

        return result;
    }

}
