package br.com.sementex.trend.trend.ui;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import br.com.sementex.trend.trend.R;
import br.com.sementex.trend.trend.access.webService.WSMethodCalls;
import br.com.sementex.trend.trend.access.dao.daoRestaurante;
import br.com.sementex.trend.trend.adapter.BackstageRecyclerAdapter;
import br.com.sementex.trend.trend.adapter.RestaurantesRecyclerAdapter;
import br.com.sementex.trend.trend.model.Restaurante;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, BackstageRecyclerAdapter.AdapterCallback, RestaurantesRecyclerAdapter.AdapterCallback, SwipeRefreshLayout.OnRefreshListener {

    private ArrayList<Restaurante> estabList;
    private BackstageRecyclerAdapter recyclerAdapter;
    private RestaurantesRecyclerAdapter recyclerRestAdapter;
    private RecyclerView recyclerView;
    private RecyclerView recyclerRest;
    private TextView toolbarTitle;

    private LinearLayout ll_praVoce;
    private LinearLayout ll_explore;
    private LinearLayout ll_premium;

    private ViewPager vp_pravoce;
    private ViewPager vp_explore;
    private ViewPager vp_premium;

    private TextView tv_praVoce;
    private TextView tv_explore;
    private TextView tv_premium;

    private ImageView iv_pravoce;
    private ImageView iv_explore;
    private ImageView iv_premium;

    private EditText et_search;

    private WSMethodCalls ws;
    private ArrayList<Restaurante> al_rest;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        instanciarElementos();
        preencherList(0);
        backStageRecyclerAdapter();
        restauranteRecyclerAdapter();
        onClickListener();

        mSwipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                mSwipeRefreshLayout.setRefreshing(true);
                preencherList(1);
            }
        });

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        Typeface myCustomFont = Typeface.createFromAsset(getAssets(), "fonts/big_river_script_sample.ttf");
        toolbarTitle.setTypeface(myCustomFont);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_camera) {

        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    public void instanciarElementos(){
        ws = new WSMethodCalls(getApplicationContext());

        mSwipeRefreshLayout = findViewById(R.id.swipe_container);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);

        //mSwipeRefreshLayout.post(new Runnable() {
        //    @Override
        //    public void run() {
        //        mSwipeRefreshLayout.setRefreshing(true);
        //        preencherList(1);
        //    }
        //});

        estabList = new ArrayList<>();
        toolbarTitle = findViewById(R.id.toolbar_title);
        recyclerView = new RecyclerView(getApplicationContext());
        recyclerView = findViewById(R.id.recycler);

        recyclerRest = findViewById(R.id.recyclerRest);

        ll_praVoce = findViewById(R.id.ll_praVoce);
        ll_explore = findViewById(R.id.ll_explore);
        ll_premium = findViewById(R.id.ll_premium);

        vp_pravoce = findViewById(R.id.vp_pravoce);
        vp_explore = findViewById(R.id.vp_explore);
        vp_premium = findViewById(R.id.vp_premium);

        tv_praVoce = findViewById(R.id.tv_praVoce);
        tv_explore = findViewById(R.id.tv_explore);
        tv_premium = findViewById(R.id.tv_premium);

        iv_pravoce = findViewById(R.id.iv_pravoce);
        iv_explore = findViewById(R.id.iv_explore);
        iv_premium = findViewById(R.id.iv_premium);

        et_search = findViewById(R.id.et_search);

    }

    public void onClickListener(){
        ll_praVoce.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //region Para Você
                //vp_pravoce.setVisibility(View.VISIBLE);
                tv_praVoce.setTextColor(Color.WHITE);
                iv_pravoce.setColorFilter(Color.WHITE);
                //endregion

                //region Explore
                //vp_explore.setVisibility(View.INVISIBLE);
                tv_explore.setTextColor(Color.parseColor("#474747"));
                iv_explore.setColorFilter(Color.parseColor("#474747"));
                //endregion

                //region Premium
                //vp_premium.setVisibility(View.INVISIBLE);
                tv_premium.setTextColor(Color.parseColor("#474747"));
                iv_premium.setColorFilter(Color.parseColor("#474747"));
                //endregion
            }
        });
        ll_explore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //region Explore
                //vp_explore.setVisibility(View.VISIBLE);
                tv_explore.setTextColor(Color.WHITE);
                iv_explore.setColorFilter(Color.WHITE);
                //endregion

                //region Para Você
                //vp_pravoce.setVisibility(View.INVISIBLE);
                tv_praVoce.setTextColor(Color.parseColor("#474747"));
                iv_pravoce.setColorFilter(Color.parseColor("#474747"));
                //endregion

                //region Premium
                //vp_premium.setVisibility(View.INVISIBLE);
                tv_premium.setTextColor(Color.parseColor("#474747"));
                iv_premium.setColorFilter(Color.parseColor("#474747"));
                //endregion
            }
        });
        ll_premium.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //region Premium
                //vp_premium.setVisibility(View.VISIBLE);
                tv_premium.setTextColor(Color.WHITE);
                iv_premium.setColorFilter(Color.WHITE);
                //endregion

                //region Para Você
                //vp_pravoce.setVisibility(View.INVISIBLE);
                tv_praVoce.setTextColor(Color.parseColor("#474747"));
                iv_pravoce.setColorFilter(Color.parseColor("#474747"));
                //endregion

                //region Explore
                //vp_explore.setVisibility(View.INVISIBLE);
                tv_explore.setTextColor(Color.parseColor("#474747"));
                iv_explore.setColorFilter(Color.parseColor("#474747"));
                //endregion
            }
        });
    }

    public void restauranteRecyclerAdapter(){
        RecyclerView.LayoutManager layoutManagerRest = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        recyclerRest.setLayoutManager(layoutManagerRest);
        recyclerRest.setHasFixedSize(true);
        recyclerRestAdapter = new RestaurantesRecyclerAdapter(MainActivity.this,MainActivity.this, getApplicationContext(), al_rest);
        recyclerRest.setAdapter(recyclerRestAdapter);

    }

    public void backStageRecyclerAdapter(){
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerAdapter = new BackstageRecyclerAdapter(MainActivity.this,MainActivity.this, getApplicationContext(), al_rest);
        recyclerView.setAdapter(recyclerAdapter);
    }

    public ArrayList<Restaurante> preencherList(int request){
        daoRestaurante daoRest = new daoRestaurante(getApplicationContext());
        switch (request){
            case 0:
                al_rest = daoRest.retornaRestaurantes();
                if(al_rest.size() < 1){
                    daoRest.apagarRestaurantes(al_rest);
                    al_rest = ws.RetornaRestaurantesTask();
                    for (Restaurante r : al_rest) {
                        daoRest.adicionarRestaurantes(r);
                    }
                }
                break;
            case 1:
                daoRest.apagarRestaurantes(al_rest);
                al_rest = ws.RetornaRestaurantesTask();
                for (Restaurante r : al_rest) {
                    daoRest.adicionarRestaurantes(r);
                }
                if(al_rest.size() > 1){
                    mSwipeRefreshLayout.setRefreshing(false);
                }
                break;
        }


        return  al_rest;
    }

    private Dialog perfil()  {
        LayoutInflater inflater = MainActivity.this.getLayoutInflater();
        @SuppressLint("InflateParams") final View v = inflater.inflate(R.layout.dialog_perfil, null);

        SimpleDateFormat formataData = new SimpleDateFormat("yyyy-MM-dd");
        Date data = new Date();
        final String dataFormatada = formataData.format(data);

        final TextView tvTitulo = v.findViewById(R.id.tv_Titulo);

        //final Spinner spinner = v.findViewById(R.id.spinner_sit);

        final AlertDialog d = new AlertDialog.Builder(MainActivity.this).setView(v).setCancelable(true).create();
        d.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                //tvTitulo.setText("Nova medição");
                //
                //ArrayList<String> listaDialog = new ArrayList<>();
                //listaDialog.clear();
                //listaDialog.add("Impedimento de leitura");
                //listaDialog.add("Leitura implausível");
                //listaDialog.add("Situação de risco");
                //listaDialog.add("Releitura");
                //listaDialog.add("Suspeita de fraude");
//
                //ArrayAdapter adapter = new ArrayAdapter(MainActivity.this, android.R.layout.simple_spinner_item, listaDialog);
                //adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                //spinner.setAdapter(adapter);

                //d.dismiss();
            }
        });

        return d;
    }

    @Override
    public void onbackstageSelecionadoResult(Restaurante c, int position) {
        perfil().show();
    }

    @Override
    public void onRestauranteSelecionadoResult(Restaurante est, int adapterPosition) {

    }

    @Override
    public void onRefresh() {
        preencherList(1);
    }


}
