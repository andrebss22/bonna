package br.com.sementex.trend.trend.access.database;

public abstract class DBStrings {

    //region TABELA RESTAURANTES
    public static final String TbRest = "TbRest";
    public static final String IDRestauranteTbRest = "IDRestaurante";
    public static final String NomeTbRest = "Nome";
    public static final String DescricaoTbRest = "Descricao";
    public static final String RotacaoSNTbRest = "RotacaoSN";
    public static final String PremiumSNTbRest = "PremiumSN";
    public static final String AtivoSNTbRest = "AtivoSN";
    public static final String CliquesTbRest = "Cliques";
    public static final String ImagemTbRest = "Imagem";
    public static final String DataUploadTbRest = "DataUpload";


    static final String sqlCreateTableTbRest = "CREATE TABLE IF NOT EXISTS " + TbRest + "(" +
            IDRestauranteTbRest + " INTEGER PRIMARY KEY AUTOINCREMENT," +
            CliquesTbRest + " INTEGER NULL DEFAULT 0," +
            NomeTbRest + " VARCHAR NOT NULL," +
            DescricaoTbRest + " VARCHAR NOT NULL," +
            ImagemTbRest + " VARCHAR NOT NULL," +
            RotacaoSNTbRest + " BIT DEFAULT (0)," +
            PremiumSNTbRest + " BIT DEFAULT (0)," +
            AtivoSNTbRest + " BIT DEFAULT (0)," +
            DataUploadTbRest + " VARCHAR NOT NULL" +
            ");";
    //endregion

    //region TABELA CONTATOS
    public static final String TbContatos = "TbContatos";
    public static final String IDContatoTbContatos = "IDContato";
    public static final String IDRestauranteTbContatos = "IDRestaurante";
    public static final String DescricaoTbContatos = "Descricao";
    public static final String FixoSNTbContatos = "FixoSN";
    public static final String NumeroTbContatos = "Numero";

    static final String sqlCreateTableTbContatos = "CREATE TABLE IF NOT EXISTS " + TbContatos + "(" +
            IDContatoTbContatos + " INTEGER PRIMARY KEY AUTOINCREMENT," +
            IDRestauranteTbContatos + " INTEGER NULL DEFAULT 0," +
            DescricaoTbContatos + " VARCHAR NOT NULL," +
            FixoSNTbContatos + " BIT DEFAULT (0)," +
            NumeroTbContatos + "  VARCHAR NULL" +
            ");";
    //endregion

    //region TABELA ENDERECOS
    public static final String TbEnderRest = "TbEnderRest";
    public static final String IDEnderecoTbEnderRest = "IDEndereco";
    public static final String IDRestauranteTbEnderRest = "IDRestaurante";
    public static final String DescricaoTbEnderRest = "Descricao";
    public static final String LogradouroTbEnderRest = "Logradouro";
    public static final String NumeroTbEnderRest = "Numero";
    public static final String CEPTbEnderRest = "CEP";
    public static final String BairroTbEnderRest = "Bairro";
    public static final String CidadeTbEnderRest = "Cidade";
    public static final String EstadoTbEnderRest = "Estado";
    public static final String MatrizSNTbEnderRest = "MatrizSN";

    static final String sqlCreateTableTbEnderRest = "CREATE TABLE IF NOT EXISTS " + TbEnderRest + "(" +
            IDEnderecoTbEnderRest + " INTEGER PRIMARY KEY AUTOINCREMENT," +
            IDRestauranteTbEnderRest + " INTEGER NULL DEFAULT 0," +
            DescricaoTbEnderRest + " VARCHAR NOT NULL," +
            LogradouroTbEnderRest + " VARCHAR NOT NULL," +
            BairroTbEnderRest + " VARCHAR NOT NULL," +
            CidadeTbEnderRest + " VARCHAR NOT NULL," +
            EstadoTbEnderRest + " VARCHAR NOT NULL," +
            NumeroTbEnderRest + " INTEGER NULL DEFAULT 0," +
            CEPTbEnderRest + " INTEGER NULL DEFAULT 0," +
            MatrizSNTbEnderRest + " BIT DEFAULT (0)" +
            ");";
    //endregion

    //************************************************************************************************************************
    // Alterações no Banco de Dados, Necessário alterar versão
    //************************************************************************************************************************


    //************************************************************************************************************************
    // Alterações no Banco de Dados, Necessário alterar versão
    //************************************************************************************************************************




}
