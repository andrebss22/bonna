package br.com.sementex.trend.trend.access.webService.task;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;

import br.com.sementex.trend.trend.access.webService.WSMethodCalls;
import br.com.sementex.trend.trend.model.Endereco;
import br.com.sementex.trend.trend.model.Restaurante;

import static br.com.sementex.trend.trend.access.model.WebService.NAMESPACE;
import static br.com.sementex.trend.trend.access.model.WebService.SOAP_ADRESS;
import static br.com.sementex.trend.trend.access.model.WebService.TIMEOUT;
import static br.com.sementex.trend.trend.access.model.WebService.URL_SOAP;

/**
 * Created by Renan Azalim on 22/06/2018.
 */

public class RetornaEnderecosTask extends AsyncTask<String, String, ArrayList<Endereco>> {
    boolean erro = false;
    String tipoErro = null;
    Context cont;
    private WSMethodCalls ws;

    public RetornaEnderecosTask(Context cont) {
        this.cont = cont;
    }

    @Override
    protected void onProgressUpdate(String... values) {
    }

    @Override
    protected ArrayList<Endereco> doInBackground(String... params) {
        Log.i("TASK I  Retorna Modulos", "Executanto Task");
        ArrayList<Endereco> retorno = new ArrayList<>();

        try {
            retorno = RetornaEnderecos();
        } catch (Exception e) {
            Log.i("TASK I Restaurantes", "Exception - Erro na execução da Task");
            if(e.toString().contains("UnknownHostException") || e.toString().contains("EOFException") || e.toString().contains("SocketTimeoutException")){
                tipoErro = "Não foi possível conectar com o servidor!";
                retorno = null;
            }
            erro = true;
            Log.e(this.getClass().getSimpleName(),e.getMessage(), e);
        }
        return retorno;
    }

    public ArrayList<Endereco> RetornaEnderecos() throws IOException, XmlPullParserException {
        ws = new WSMethodCalls(cont);
        final String METHOD_NAME = "RetornaEnderecos";
        final String SOAP_ACTION = SOAP_ADRESS + METHOD_NAME;

        ArrayList<Endereco> lista = new ArrayList<>();
        Log.i("Serviço", METHOD_NAME + " - Inicializando serviço ...");

        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
        //request.addProperty("IdSmtUsuario", IdSmtUsuario);
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);
        envelope.addMapping(NAMESPACE, METHOD_NAME, new Endereco().getClass());
        SoapObject response;
        ArrayList<HeaderProperty> headerPropertyArrayList = new ArrayList<>();
        headerPropertyArrayList.add(new HeaderProperty("Connection", "close"));
        HttpTransportSE androidHttpTransport = new HttpTransportSE(URL_SOAP, TIMEOUT);
        //androidHttpTransport.debug = true;
        androidHttpTransport.call(SOAP_ACTION, envelope);
        //System.out.println(androidHttpTransport.requestDump);
        //System.out.println(androidHttpTransport.responseDump);
        response = (SoapObject) envelope.getResponse();


        if (response != null) {
            if(response.getPropertyCount() > 0) {
                for (int i = 0; i < response.getPropertyCount(); i++) {
                    SoapObject us = (SoapObject) response.getProperty(i);
                    Endereco end = new Endereco();

                    end.setBairro(us.getProperty("Bairro").toString());
                    end.setCEP(Integer.parseInt(us.getProperty("CEP").toString()));
                    end.setCidade(us.getProperty("Cidade").toString());
                    end.setDescricao(us.getProperty("Descricao").toString());
                    end.setEstado(us.getProperty("Estado").toString());
                    end.setIDEndereco(Integer.parseInt(us.getProperty("IDEndereco").toString()));
                    end.setIDRestaurante(Integer.parseInt(us.getProperty("IDRestaurante").toString()));
                    end.setLogradouro(us.getProperty("Logradouro").toString());
                    end.setMatrizSN(Boolean.parseBoolean(us.getProperty("MatrizSN").toString()));
                    end.setNumero(Integer.parseInt(us.getProperty("Numero").toString()));
                    lista.add(end);
                }
            }
        } else if (response == null) {
            Log.i("Serviço", METHOD_NAME + " - FIM - Lista não Preenchida");
            lista = null;
        }
        return lista;
    }
}
