package br.com.sementex.trend.trend.access.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.SQLException;
import android.util.Log;
import br.com.sementex.trend.trend.access.database.DB;
import br.com.sementex.trend.trend.access.database.DBStrings;
import br.com.sementex.trend.trend.model.Contato;

public class daoContato {
    private DB conexao;

    public daoContato(Context context) {
        this.conexao = DB.getInstance(context);
    }

    public boolean adicionarContatos(Contato c) {
        long insertOk;
        boolean result = true;
        try {
            conexao.getWritableDatabase().beginTransaction();
            ContentValues values = new ContentValues();
            values.put(DBStrings.IDContatoTbContatos, c.getIDContato());
            values.put(DBStrings.IDRestauranteTbContatos, c.getIDRestaurante());
            values.put(DBStrings.DescricaoTbContatos, c.getDescricao());
            values.put(DBStrings.FixoSNTbContatos, c.isFixoSN());
            values.put(DBStrings.NumeroTbContatos, c.getNumero());

            try {
                insertOk = conexao.getWritableDatabase().insert(DBStrings.TbContatos, null, values);
                if (insertOk == -1) {
                    result = false;
                }
            } catch (Exception e) {
                Log.e(this.getClass().getSimpleName(), e.getMessage(), e);
                result = false;
            }
            conexao.getWritableDatabase().setTransactionSuccessful();

            return result;
        } catch (SQLException e) {
            Log.e("DBStrings", e.getMessage(), e);
            return false;
        } finally {
            conexao.getWritableDatabase().endTransaction();
            conexao.close();
        }
    }
}
