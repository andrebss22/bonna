package br.com.sementex.trend.trend.access.model;

public class WebService {

    public WebService(){
    }
    public static final String URL = "http://sementex.com/api/Service1.svc";
    public static final String SOAP_WCF = "/Soap";
    public static final String REST_WCF = "/rest";
    public static final String NAMESPACE = "http://sementex.com/wcf/";
    public static final String SOAP_ADRESS = "http://sementex.com/wcf/IService1/";
    public static final int TIMEOUT = 50000;

    public static final String URL_SOAP = URL + SOAP_WCF;
    public static final String URL_REST = URL + REST_WCF;


}
