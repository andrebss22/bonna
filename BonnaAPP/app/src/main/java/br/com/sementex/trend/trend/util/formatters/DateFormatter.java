package br.com.sementex.trend.trend.util.formatters;

import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;


public class DateFormatter {

    public static final String DOMINGO = "D";
    public static final String SEGUNDA = "2";
    public static final String TERCA = "3";
    public static final String QUARTA = "4";
    public static final String QUINTA = "5";
    public static final String SEXTA = "6";
    public static final String SABADO = "S";

    private DateFormatter() {
    }

    //FUNÇÃO PARA RETORNAR A DATA COMO UMA STRING
    public static String formatarData(Date data){
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
        try {
            return sdf.format(data);
        } catch (Exception ex) {
            Log.e("formatarData", ex.getMessage(), ex);
            return "01/01/1900";
        }
    }

   public static String formatarDataDiaMes(Date data){
       SimpleDateFormat sdf = new SimpleDateFormat("dd/MM", Locale.getDefault());
       try {
           return sdf.format(data);
       } catch (Exception ex) {
           Log.e("formatarDataDiaMes", ex.getMessage(), ex);
           return "01/01";
       }
    }

   public static int daysBetween(Date d1, Date d2){
       long diff = d2.getTime() - d1.getTime();
       return (int)TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
    }

    public static Date parseISODate(String sDate) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        try {
            return sdf.parse(sDate);
        } catch (ParseException e) {
            Log.e("parseISODate",e.getMessage(), e);
            Calendar tmp = Calendar.getInstance();
            tmp.setTimeInMillis(0);
            return tmp.getTime();
        }
    }

    public static Date parseDate(String sDate) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
        try {
            return sdf.parse(sDate);
        } catch (ParseException e) {
            Log.e("parseDate",e.getMessage(), e);
            Calendar tmp = Calendar.getInstance();
            tmp.setTimeInMillis(0);
            return tmp.getTime();
        }
    }

    public static Date parseDbDateTime(String sDate) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.getDefault());
        sdf.setTimeZone(TimeZone.getDefault());
        try {
            return sdf.parse(sDate);
        } catch (ParseException e) {
            SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.getDefault());
            sdf1.setTimeZone(TimeZone.getDefault());
            try {
                return sdf1.parse(sDate);
            } catch (ParseException e1) {
                SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault());
                sdf2.setTimeZone(TimeZone.getDefault());
                try {
                    return sdf2.parse(sDate);
                } catch (ParseException e2) {
                    SimpleDateFormat sdf3 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                    sdf3.setTimeZone(TimeZone.getDefault());
                    try {
                        return sdf3.parse(sDate);
                    } catch (ParseException e3) {
                        Log.w("parseDbDateTime", e3.getMessage());
                        return DateFormatter.parseISODate("1900-01-01");
                    }
                }

            }
        }
    }

    public static String formatISODate(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        if (date != null) {
            return sdf.format(date);
        } else {
            return "1900-01-01";
        }
    }

    public static String formatISODateWithTime(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'00:00:00", Locale.getDefault());
        if (date != null) {
            return sdf.format(date);
        } else {
            return "1900-01-01";
        }
    }

    public static String formatDBDateTime(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.getDefault());
        return sdf.format(date);
    }

    /**
     * Method will return the current day without any time information, thus the current day at midnight
     *
     * @return Date representing current date at midnight
     */
    public static Date currentDate() {
        Calendar now = Calendar.getInstance();
        now.set(Calendar.HOUR_OF_DAY, 0);
        now.set(Calendar.MINUTE, 0);
        now.set(Calendar.SECOND, 0);
        now.set(Calendar.MILLISECOND, 0);
        return now.getTime();
    }

    public static Date currentDBDateAndTime() {
        Calendar now = Calendar.getInstance();
        return now.getTime();
    }

    public static Date calcularPeriodoAtrasoDataFaturamento(Date data, int dias) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(data);
        calendar.add(Calendar.DAY_OF_YEAR, dias);
        return calendar.getTime();
    }

    public static String getDayOfTheWeek() {
        String dia;
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        int dayWeek = cal.get(Calendar.DAY_OF_WEEK);

        switch (dayWeek) {
            case 1:
                dia = DOMINGO;
                break;
            case 2:
                dia = SEGUNDA;
                break;
            case 3:
                dia = TERCA;
                break;
            case 4:
                dia = QUARTA;
                break;
            case 5:
                dia = QUINTA;
                break;
            case 6:
                dia = SEXTA;
                break;
            case 7:
                dia = SABADO;
                break;
            default:
                dia = DOMINGO;
        }
        return dia;
    }

}
