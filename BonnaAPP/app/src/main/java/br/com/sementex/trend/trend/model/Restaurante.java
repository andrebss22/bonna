package br.com.sementex.trend.trend.model;

import java.io.Serializable;

public class Restaurante implements Serializable {

    public Restaurante(){
    }

    private int IDRestaurante;
    private String Imagem;
    private String Nome;
    private String Descricao;
    private int Cliques;
    private boolean RotacaoSN;
    private boolean PremiumSN;
    private boolean AtivoSN;

    public int getIDRestaurante() {
        return IDRestaurante;
    }

    public void setIDRestaurante(int IDRestaurante) {
        this.IDRestaurante = IDRestaurante;
    }

    public String getImagem() {
        return Imagem;
    }

    public void setImagem(String imagem) {
        Imagem = imagem;
    }

    public String getNome() {
        return Nome;
    }

    public void setNome(String nome) {
        Nome = nome;
    }

    public String getDescricao() {
        return Descricao;
    }

    public void setDescricao(String descricao) {
        Descricao = descricao;
    }

    public int getCliques() {
        return Cliques;
    }

    public void setCliques(int cliques) {
        Cliques = cliques;
    }

    public boolean isRotacaoSN() {
        return RotacaoSN;
    }

    public void setRotacaoSN(boolean rotacaoSN) {
        RotacaoSN = rotacaoSN;
    }

    public boolean isPremiumSN() {
        return PremiumSN;
    }

    public void setPremiumSN(boolean premiumSN) {
        PremiumSN = premiumSN;
    }

    public boolean isAtivoSN() {
        return AtivoSN;
    }

    public void setAtivoSN(boolean ativoSN) {
        AtivoSN = ativoSN;
    }
}
