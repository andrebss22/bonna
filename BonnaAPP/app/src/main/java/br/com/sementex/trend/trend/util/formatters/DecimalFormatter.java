package br.com.sementex.trend.trend.util.formatters;

import android.content.Context;
import android.util.Log;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

import br.com.sementex.trend.trend.R;

public class DecimalFormatter {

    private static final Locale BR = new Locale("pt", "BR");
    private static final Locale US = new Locale("en", "US");
    private static final NumberFormat FORMAT_BR = NumberFormat.getInstance(BR);
    private static final NumberFormat FORMAT_US = NumberFormat.getInstance(US);
    private static DecimalFormat fmt;
    private Context context;
    private static final String TAG = "DecimalFormatter";


    public DecimalFormatter(Context context) {
        this.context = context;
        fmt = new DecimalFormat("###,###.00");
        Locale.setDefault(new Locale("pt", "BR"));
    }

    //FUNÇÃO RESPONSÁVEL POR ADICIONAR DUAS CASAS DECIMAIS E RETORNAR UMA STRING
    public static String duasCasasDecimaisToString(Double n) {
        NumberFormat nf = new DecimalFormat("###,##0.00");
        return nf.format(n).replace(',', ',');
    }

    public static String porcentagemUmaCasaDecimal(Double n) {
        NumberFormat nf = new DecimalFormat("###,##0.0");
        return nf.format(n).replace(',', ',') + 0;
    }

    public static Double porcentagemUmaCasaDecimal(String n) {
        NumberFormat nf = new DecimalFormat("###,##0.0");
        Double result = null;
        try {
            result = nf.parse(n).doubleValue();
        } catch (ParseException e) {
            Log.e(TAG ,e.getMessage(), e);
        }
        return result;
    }

    public static String duasCasasDecimaisToString2(Double num) {
        DecimalFormatSymbols custom = new DecimalFormatSymbols(Locale.getDefault());
        custom.setDecimalSeparator(',');
        fmt.setDecimalFormatSymbols(custom);
        System.out.println(fmt.format(num));
        return fmt.format(num);
    }

    //FUNÇÃO RESPONSÁVEL POR ADICIONAR DUAS CASAS DECIMAIS E RETORNAR UM DOUBLE
    public static Double twoDecimalPlaces(Double num) {
        DecimalFormat df;
        df = new DecimalFormat("0.00");
        return Double.parseDouble(df.format(num).replace(',', '.'));
    }

    //FUNÇÃO RESPONSÁVEL POR ADICIONAR UMA CASA DECIMAL E RETORNAR UMA STRING
    public static String umaCasaDecimalToString(Double n) {
        NumberFormat nf;
        nf = new DecimalFormat("###,##0.0");
        return nf.format(n).replace(',', ',');
    }

    //FUNÇÃO RESPONSÁVEL POR ADICIONAR UMA CASA DECIMAL E RETORNAR UM DOUBLE
    public static Double umaCasaDecimalToDouble(Double n) {
        try {
            NumberFormat nf = new DecimalFormat("#####0.0");
            return Double.valueOf(nf.format(n));
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return (double) Math.round(n);
        }

    }


    //FUNÇÃO RESPONSÁVEL POR ADICIONAR A MOEDA NO VALOR
    public static String adicionaMoeda(Context context, Double d) {
        return ((d < 0) ? "- " : "") + context.getString(R.string.moeda_pt_br) + duasCasasDecimaisToString(((d < 0) ? d * -1 : d));
    }

    public String formataDecimal(Double d) {
        return ((d < 0) ? "- " : "") + duasCasasDecimaisToString(((d < 0) ? d * -1 : d));
    }

    //FUNÇÃO RESPONSÁVEL POR RETIRAR A MOEDA DO VALOR
    public Double retiraMoeda(String s) {
        if (s.startsWith(context.getString(R.string.moeda_pt_br))) {
            return Double.valueOf(s.substring(context.getString(R.string.moeda_pt_br).length(), s.length()));
        } else if (s.startsWith("- " + context.getString(R.string.moeda_pt_br))) {
            return (Double.valueOf(s.substring(("- " + context.getString(R.string.moeda_pt_br)).length(), s.length()))) * -1;
        } else {
            return 0.00;
        }
    }

    public static double universalParser(String number) throws ParseException {
        int dotIndex = number.indexOf('.');
        int commaIndex = number.indexOf(',');
        FORMAT_BR.setMaximumFractionDigits(1);
        if (dotIndex < commaIndex) {
            return FORMAT_BR.parse(number).doubleValue();
        } else {
            return FORMAT_US.parse(number).doubleValue();
        }
    }

    public static String formatDecimal(Double number) throws ParseException {

        NumberFormat numeroBR = NumberFormat.getNumberInstance(BR);
        numeroBR.setMinimumFractionDigits(1);
        numeroBR.setMaximumFractionDigits(1);
        return numeroBR.format(number);
    }


}
