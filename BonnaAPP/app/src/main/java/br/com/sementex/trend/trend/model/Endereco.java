package br.com.sementex.trend.trend.model;

public class Endereco {

    public Endereco(){
    }

    private int IDEndereco;
    private int IDRestaurante;
    private String Descricao;
    private String Logradouro;
    private int Numero;
    private int CEP;
    private String Bairro;
    private String Cidade;
    private String Estado;
    private boolean MatrizSN;

    public int getIDEndereco() {
        return IDEndereco;
    }

    public void setIDEndereco(int IDEndereco) {
        this.IDEndereco = IDEndereco;
    }

    public int getIDRestaurante() {
        return IDRestaurante;
    }

    public void setIDRestaurante(int IDRestaurante) {
        this.IDRestaurante = IDRestaurante;
    }

    public String getDescricao() {
        return Descricao;
    }

    public void setDescricao(String descricao) {
        Descricao = descricao;
    }

    public String getLogradouro() {
        return Logradouro;
    }

    public void setLogradouro(String logradouro) {
        Logradouro = logradouro;
    }

    public int getNumero() {
        return Numero;
    }

    public void setNumero(int numero) {
        Numero = numero;
    }

    public int getCEP() {
        return CEP;
    }

    public void setCEP(int CEP) {
        this.CEP = CEP;
    }

    public String getBairro() {
        return Bairro;
    }

    public void setBairro(String bairro) {
        Bairro = bairro;
    }

    public String getCidade() {
        return Cidade;
    }

    public void setCidade(String cidade) {
        Cidade = cidade;
    }

    public String getEstado() {
        return Estado;
    }

    public void setEstado(String estado) {
        Estado = estado;
    }

    public boolean isMatrizSN() {
        return MatrizSN;
    }

    public void setMatrizSN(boolean matrizSN) {
        MatrizSN = matrizSN;
    }
}
