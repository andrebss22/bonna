package br.com.sementex.trend.trend.access.webService;

import android.content.Context;
import android.util.Log;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;
import java.util.concurrent.ExecutionException;

import br.com.sementex.trend.trend.access.webService.task.RetornaContatosTask;
import br.com.sementex.trend.trend.access.webService.task.RetornaEnderecosTask;
import br.com.sementex.trend.trend.access.webService.task.RetornaRestaurantesTask;
import br.com.sementex.trend.trend.model.Contato;
import br.com.sementex.trend.trend.model.Endereco;
import br.com.sementex.trend.trend.model.Restaurante;


public class WSMethodCalls {
    private Context context;
    private String url;
    private String mensagemErro;
    int tentativas = 5;
    private SimpleDateFormat sdf;


    public WSMethodCalls(Context context) {
        this.context = context.getApplicationContext();
        //url = MyPref.getInstance().read(MyPref.STR_WCF_URL, "");
        sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
    }

    public WSMethodCalls() {
    }

    //public smtUsuarioModel VerificaEmailSenhaTask(String Email, String Senha) {
    //    smtUsuarioModel acom = new smtUsuarioModel();
    //    try {
    //        for (int i = 1; i <= tentativas; i++) {
    //            mensagemErro = "";
    //            acom = new VerificaEmailSenhaTask(context).execute(Email, Senha).get();
    //            if (acom != null) {
    //                break;
    //            } else {
    //                mensagemErro = "Erro ao verificar login";
    //                break;
    //            }
    //        }
    //    } catch (InterruptedException | ExecutionException e) {
    //        Log.e(this.getClass().getSimpleName(),e.getMessage(), e);
    //    }
    //    return acom;
    //}

    //public String SalvarUsuarioSementeTask(smtUsuarioModel usuario) {
//
    //    String result = "";
    //    try {
    //        result = new SalvarUsuarioSementeTask(context, usuario).execute().get();
//
    //    } catch (InterruptedException | ExecutionException e) {
    //        Log.e(this.getClass().getSimpleName(),e.getMessage(), e);
    //        result = e.toString();
    //    }
    //    return result;
    //}

    public ArrayList<Restaurante> RetornaRestaurantesTask() {
        ArrayList<Restaurante> list = new ArrayList<>();
        try {
            for (int i = 1; i <= tentativas; i++) {
                mensagemErro = "";
                list = new RetornaRestaurantesTask(context).execute().get();
                if (list != null) {
                    break;
                } else {
                    mensagemErro = "Erro ao buscar restaurantes";
                    break;
                }
            }
        } catch (InterruptedException | ExecutionException e) {
            Log.e(this.getClass().getSimpleName(),e.getMessage(), e);
        }
        return list;
    }

    public ArrayList<Contato> RetornaContatosTask() {
        ArrayList<Contato> list = new ArrayList<>();
        try {
            for (int i = 1; i <= tentativas; i++) {
                mensagemErro = "";
                list = new RetornaContatosTask(context).execute().get();
                if (list != null) {
                    break;
                } else {
                    mensagemErro = "Erro ao buscar contatos";
                    break;
                }
            }
        } catch (InterruptedException | ExecutionException e) {
            Log.e(this.getClass().getSimpleName(),e.getMessage(), e);
        }
        return list;
    }

    public ArrayList<Endereco> RetornaEnderecosTask() {
        ArrayList<Endereco> list = new ArrayList<>();
        try {
            for (int i = 1; i <= tentativas; i++) {
                mensagemErro = "";
                list = new RetornaEnderecosTask(context).execute().get();
                if (list != null) {
                    break;
                } else {
                    mensagemErro = "Erro ao buscar enderecos";
                    break;
                }
            }
        } catch (InterruptedException | ExecutionException e) {
            Log.e(this.getClass().getSimpleName(),e.getMessage(), e);
        }
        return list;
    }


}
