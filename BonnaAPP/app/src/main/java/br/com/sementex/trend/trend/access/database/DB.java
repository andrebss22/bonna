package br.com.sementex.trend.trend.access.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DB extends SQLiteOpenHelper {
    private static final String BASE = "BONNABD";
    private static DB DBInstance;
    private static final int VERSAO = 1;

    public static synchronized DB getInstance(Context cont) {
        if (DBInstance == null) {
            DBInstance = new DB(cont.getApplicationContext());
        }
        return DBInstance;
    }

    private DB(Context context) {
        super(context, BASE, null, VERSAO);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DBStrings.sqlCreateTableTbRest);
        db.execSQL(DBStrings.sqlCreateTableTbContatos);
        db.execSQL(DBStrings.sqlCreateTableTbEnderRest);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.i("SQLite", "Chamando on Upgrade");

            //if (oldVersion < 2) {
           //
            //}

    }

}



