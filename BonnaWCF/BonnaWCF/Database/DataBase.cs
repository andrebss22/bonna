﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace BonnaWCF.DataBase
{
    public class DataBaseConnection : IDisposable
    {
        private SqlDataAdapter Adapter;
        private SqlConnection conn;

        public void Dispose()
        {
            if (Adapter != null)
            {
                Adapter.Dispose();
                Adapter = null;
            }
            if (conn != null)
            {
                conn.Dispose();
                conn = null;
            }
        }

        public DataBaseConnection()
        {
            Adapter = new SqlDataAdapter();
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
        }

        private SqlConnection AbrirConexao()
        {
            if (conn.State == ConnectionState.Closed || conn.State == ConnectionState.Broken)
            {
                conn.Open();
            }
            return conn;
        }

        public DataTable RetornaDataTable(String _query)
        {
            SqlCommand comando = new SqlCommand();
            DataTable dataTable = new DataTable();
            dataTable = null;
            DataSet ds = new DataSet();
            try
            {
                comando.Connection = AbrirConexao();
                comando.CommandText = _query;
                comando.ExecuteNonQuery();
                Adapter.SelectCommand = comando;
                Adapter.Fill(ds);
                dataTable = ds.Tables[0];
            }
            catch (SqlException e)
            {
                Console.Write(string.Format("Erro - Connection.executeSelectQuery - Query: {0} \nException: {1}", _query, e.StackTrace));
                return null;
            }
            finally
            {
                try
                {

                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }

            }
            return dataTable;
        }

        public bool Insert(String _query)
        {
            SqlCommand comando = new SqlCommand();
            try
            {
                comando.Connection = AbrirConexao();
                comando.CommandText = _query;
                Adapter.InsertCommand = comando;
                comando.ExecuteNonQuery();
            }
            catch (SqlException e)
            {
                Console.Write(string.Format("Erro - Connection.executeInsertQuery - Query: {0} \nException: \n{1}", _query, e.StackTrace));
                return false;
            }
            finally
            {
            }
            return true;
        }

        public bool Update(String _query)
        {
            SqlCommand comando = new SqlCommand();
            try
            {
                comando.Connection = AbrirConexao();
                comando.CommandText = _query;
                Adapter.UpdateCommand = comando;
                comando.ExecuteNonQuery();
            }
            catch (SqlException e)
            {
                Console.Write(string.Format("Erro - Connection.executeUpdateQuery - Query: {0} \nException: {1}", _query, e.StackTrace));
                return false;
            }
            finally
            {
                comando.Connection.Close();
            }
            return true;
        }

        public bool Delete(String _query)
        {
            SqlCommand comando = new SqlCommand();
            try
            {
                comando.Connection = AbrirConexao();
                comando.CommandText = _query;
                Adapter.DeleteCommand = comando;
                comando.ExecuteNonQuery();
            }
            catch (SqlException e)
            {
                Console.Write(string.Format("Erro - Connection.executeDeleteQuery - Query: {0} \nException: \n{1}", _query, e.StackTrace));
                return false;
            }
            finally
            {
            }
            return true;
        }

        public bool Execute(String _query)
        {
            SqlCommand comando = new SqlCommand();
            try
            {
                comando.Connection = AbrirConexao();
                comando.CommandText = _query;
                comando.ExecuteNonQuery();
            }
            catch (SqlException e)
            {
                Console.Write(string.Format("Erro - Connection.executeUpdateQuery - Query: {0} \nException: {1}", _query, e.StackTrace));
                return false;
            }
            finally
            {
                comando.Connection.Close();
            }
            return true;
        }


        public int Select(String _query)
        {
            SqlCommand comando = new SqlCommand();
            int resultado = 0;
            try
            {
                comando.Connection = AbrirConexao();
                comando.CommandText = _query;
                SqlDataReader reader = comando.ExecuteReader();
                while (reader.Read())
                {
                    resultado = Convert.ToInt32(reader[0]);
                }

            }
            catch (SqlException e)
            {
                Console.Write(string.Format("Erro - Connection.executeSelectQuery - Query: {0} \nException: {1}", _query, e.StackTrace));
                return 1;
            }
            finally
            {
                comando.Connection.Close();
            }
            return resultado;
        }


        public decimal SelectDecimal(String _query)
        {
            SqlCommand comando = new SqlCommand();
            decimal valido = 1;
            try
            {
                comando.Connection = AbrirConexao();
                comando.CommandText = _query;
                SqlDataReader reader = comando.ExecuteReader();
                while (reader.Read())
                {
                    valido = Convert.ToDecimal(reader[0]);
                }

            }
            catch (SqlException e)
            {
                Console.Write(string.Format("Erro - Connection.executeUpdateQuery - Query: {0} \nException: {1}", _query, e.StackTrace));
                return 1;
            }
            finally
            {
                comando.Connection.Close();
            }
            return valido;
        }

        public string SelectString(String _query)
        {
            SqlCommand comando = new SqlCommand();
            string valido = null;
            try
            {
                comando.Connection = AbrirConexao();
                comando.CommandText = _query;
                SqlDataReader reader = comando.ExecuteReader();
                while (reader.Read())
                {
                    valido = Convert.ToString(reader[0]);
                }

            }
            catch (SqlException e)
            {
                Console.Write(string.Format("Erro - Connection.executeUpdateQuery - Query: {0} \nException: {1}", _query, e.StackTrace));
                return null;
            }
            finally
            {
                comando.Connection.Close();
            }
            return valido;
        }

        public bool SelectBool(String _query)
        {
            SqlCommand comando = new SqlCommand();
            bool valido = true;
            try
            {
                comando.Connection = AbrirConexao();
                comando.CommandText = _query;
                SqlDataReader reader = comando.ExecuteReader();
                while (reader.Read())
                {
                    valido = Convert.ToBoolean(reader[0]);
                }

            }
            catch (SqlException e)
            {
                Console.Write(string.Format("Erro - Connection.executeUpdateQuery - Query: {0} \nException: {1}", _query, e.StackTrace));
                return true;
            }
            finally
            {
                comando.Connection.Close();
            }
            return valido;
        }

        public DataSet RetornaDataSet(String _query)
        {
            SqlCommand comando = new SqlCommand();
            DataSet dataSet = new DataSet();
            try
            {

                comando.Connection = AbrirConexao();
                comando.CommandText = _query;
                Adapter.SelectCommand = comando;
                Adapter.Fill(dataSet, "Tabela");

            }
            catch (SqlException e)
            {
                Console.Write(string.Format("Erro - Connection.executeUpdateQuery - Query: {0} \nException: {1}", _query, e.StackTrace));
                return null;
            }
            finally
            {
            }
            return dataSet;
        }

        public int InsertReturnID(String _query)
        {
            SqlCommand comando = new SqlCommand();
            int valido = 0;

            try
            {
                comando.Connection = AbrirConexao();
                comando.CommandText = _query;
                Adapter.InsertCommand = comando;
                //comando.ExecuteNonQuery();

                valido = (int)comando.ExecuteScalar();

            }
            catch (SqlException e)
            {
                Console.Write(string.Format("Erro - Connection.executeInsertQuery - Query: {0} \nException: \n{1}", _query, e.StackTrace));
                return 0;
            }
            finally
            {
            }
            return valido;
        }

        public int StatusBaseDados()
        {
            string _query = string.Format("SELECT state FROM sys.databases WHERE name = '{0}'", conn.Database);
            SqlCommand comando = new SqlCommand();
            int valido = 1;
            try
            {
                comando.Connection = AbrirConexao();
                comando.CommandText = _query;
                comando.ExecuteNonQuery();
                SqlDataReader reader = comando.ExecuteReader();
                while (reader.Read())
                {
                    valido = Convert.ToInt32(reader[0]);
                }
            }
            catch (SqlException e)
            {
                Console.Write(string.Format("Erro - Connection.executeUpdateQuery - Query: {0} \nException: {1}", _query, e.StackTrace));
                return 1;
            }
            finally
            {
                comando.Connection.Close();
            }
            return valido;
        }

        public string IdiomaBanco()
        {
            string query = string.Format("SELECT @@LANGUAGE AS Idioma", conn.Database);
            SqlCommand comando = new SqlCommand();
            string Idioma = "";
            try
            {
                comando.Connection = AbrirConexao();
                comando.CommandText = query;
                comando.ExecuteNonQuery();
                SqlDataReader reader = comando.ExecuteReader();
                while (reader.Read())
                {
                    Idioma = Convert.ToString(reader[0]);
                }
            }
            catch (SqlException e)
            {
                Console.Write(string.Format("Erro - Connection.executeUpdateQuery - Query: {0} \nException: {1}", query, e.StackTrace));
                return "";
            }
            finally
            {
                comando.Connection.Close();
            }
            return Idioma;
        }

    }

}