﻿using BonnaWCF.Class;
using BonnaWCF.Model.Restaurante;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace BonnaWCF
{

    [ServiceContract(Namespace = Constantes.Namespace)]
    public interface IService1
    {
        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/RetornaRestaurantes")]
        List<RestauranteModel> RetornaRestaurantes();

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/RetornaRestaurantePorId/{Id}")]
        List<RestauranteModel> RetornaRestaurantePorId(string Id);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/RetornaHorarioRestaurante")]
        List<HorarioModel> RetornaHorarioRestaurante();

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/RetornaContatoRestaurante")]
        List<ContatoModel> RetornaContatoRestaurante();

    }

}
