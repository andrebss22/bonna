﻿using BonnaWCF.Class;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace BonnaWCF.Dao.Restaurante
{
    public class RestauranteDAO : Config
    {
        //NESTA CLASSE SE ENCONTRA TODAS AS QUERIES REFERENTES A RESTAURANTE

        #region Restaurante

        #region RetornaRestaurantes
        public DataTable RetornaRestaurantes()
        {      
            string query = String.Format(@"SELECT *FROM TbRest");
            return conn.RetornaDataTable(query);
        }
        #endregion

        #region RetornaRestaurantePorId
        public DataTable RetornaRestaurantePorId(int Id)
        {
            string query = String.Format(@"SELECT *FROM TbRest WHERE IDRestaurante = {0}", Id);
            return conn.RetornaDataTable(query);
        }
        #endregion

        #endregion

        #region Contato

        #region RetornaContatoRestaurante
        public DataTable RetornaContatoRestaurante()
        {
            string query = String.Format(@"SELECT *FROM TbContatos");
            return conn.RetornaDataTable(query);
        }
        #endregion

        #endregion

        #region Endereco

        #region RetornaEnderecoRestaurante
        public DataTable RetornaEnderecoRestaurante()
        {
            string query = String.Format(@"SELECT *FROM TbEnderRest");
            return conn.RetornaDataTable(query);
        }
        #endregion

        #endregion

        #region Horario

        #region RetornaHorarioRestaurante
        public DataTable RetornaHorarioRestaurante()
        {
            string query = String.Format(@"SELECT *FROM TbHorario");
            return conn.RetornaDataTable(query);
        }
        #endregion

        #endregion

    }
}