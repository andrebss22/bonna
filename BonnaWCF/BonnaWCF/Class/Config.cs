﻿using BonnaWCF.DataBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BonnaWCF.Class
{
    public class Config : IDisposable
    {
        public DataBaseConnection conn;

        public Config()
        {
            conn = new DataBaseConnection();
        }
        public void Dispose()
        {
            if (conn != null)
            {
                conn.Dispose();
                conn = null;
            }
        }
    }
}