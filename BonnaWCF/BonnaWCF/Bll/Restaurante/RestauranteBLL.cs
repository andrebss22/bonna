﻿using BonnaWCF.Dao.Restaurante;
using BonnaWCF.Model.Restaurante;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace BonnaWCF.Bll.Restaurante
{
    public class RestauranteBLL : IDisposable
    {
        private RestauranteDAO _restDAO;

        public RestauranteBLL()
        {
            _restDAO = new RestauranteDAO();
        }

        public void Dispose()
        {
            if (_restDAO != null)
            {
                _restDAO.Dispose();
                _restDAO = null;
            }
        }

        public List<RestauranteModel> RetornaRestaurantes()
        {
            return DataValue(_restDAO.RetornaRestaurantes());
        }

       public List<RestauranteModel> RetornaRestaurantePorId(int Id)
        {
            return DataValue(_restDAO.RetornaRestaurantePorId(Id));
        }

        public List<RestauranteModel> DataValue(DataTable data)
        {
            List<RestauranteModel> list = new List<RestauranteModel>();
            foreach (DataRow dr in data.Rows)
            {
                RestauranteModel rest = new RestauranteModel();

                rest.IDRestaurante = Convert.ToInt32(dr["IDRestaurante"].ToString());
                rest.Imagem = dr["Imagem"] == DBNull.Value ? "" : dr["Imagem"].ToString();
                rest.Nome = dr["Nome"] == DBNull.Value ? "" : dr["Nome"].ToString();
                rest.Descricao = dr["Descricao"] == DBNull.Value ? "" : dr["Descricao"].ToString();
                rest.Cliques = dr["Cliques"] == DBNull.Value ? 0 : Convert.ToInt32(dr["Cliques"].ToString());
                rest.RotacaoSN = dr["RotacaoSN"] == DBNull.Value ? false : Convert.ToBoolean(dr["RotacaoSN"].ToString());
                rest.PremiumSN = dr["PremiumSN"] == DBNull.Value ? false : Convert.ToBoolean(dr["PremiumSN"].ToString());
                rest.AtivoSN = dr["AtivoSN"] == DBNull.Value ? false : Convert.ToBoolean(dr["AtivoSN"].ToString());

                list.Add(rest);
            }
            return list;
        }
    }
}