﻿using BonnaWCF.Dao.Restaurante;
using BonnaWCF.Model.Restaurante;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace BonnaWCF.Bll.Restaurante
{
    public class ContatoBLL : IDisposable
    {
        private RestauranteDAO _restDAO;

        public ContatoBLL()
        {
            _restDAO = new RestauranteDAO();
        }

        public void Dispose()
        {
            if (_restDAO != null)
            {
                _restDAO.Dispose();
                _restDAO = null;
            }
        }

        public List<ContatoModel> RetornaContatoRestaurante()
        {
            return DataValue(_restDAO.RetornaContatoRestaurante());
        }

        public List<ContatoModel> DataValue(DataTable data)
        {
            List<ContatoModel> list = new List<ContatoModel>();
            foreach (DataRow dr in data.Rows)
            {
                ContatoModel cont = new ContatoModel();
                cont.IDContato = Convert.ToInt32(dr["IDContato"].ToString());
                cont.IDRestaurante = Convert.ToInt32(dr["IDRestaurante"].ToString());
                cont.Descricao = dr["Descricao"] == DBNull.Value ? "" : dr["Descricao"].ToString();
                cont.Numero = dr["Numero"] == DBNull.Value ? 0 : Convert.ToInt32(dr["Numero"].ToString());
                cont.FixoSN = dr["FixoSN"] == DBNull.Value ? false : Convert.ToBoolean(dr["FixoSN"].ToString());

                list.Add(cont);
            }
            return list;
        }
    }
}