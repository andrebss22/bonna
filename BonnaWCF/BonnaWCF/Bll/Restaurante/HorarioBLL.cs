﻿using BonnaWCF.Dao.Restaurante;
using BonnaWCF.Model.Restaurante;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace BonnaWCF.Bll.Restaurante
{
    public class HorarioBLL : IDisposable
    {
        private RestauranteDAO _restDAO;

        public HorarioBLL()
        {
            _restDAO = new RestauranteDAO();
        }

        public void Dispose()
        {
            if (_restDAO != null)
            {
                _restDAO.Dispose();
                _restDAO = null;
            }
        }

        public List<HorarioModel> RetornaHorarioRestaurantes()
        {
            return DataValue(_restDAO.RetornaHorarioRestaurante());
        }

        public List<HorarioModel> DataValue(DataTable data)
        {
            List<HorarioModel> list = new List<HorarioModel>();
            foreach (DataRow dr in data.Rows)
            {
                HorarioModel hor = new HorarioModel();

                hor.IDHorario = Convert.ToInt32(dr["IDRestaurante"].ToString());
                hor.IDRestaurante = Convert.ToInt32(dr["IDRestaurante"].ToString());
                hor.Dia = dr["Dia"] == DBNull.Value ? "" : dr["Dia"].ToString();
                hor.HoraAbertura = dr["HoraAbertura"] == DBNull.Value ? "" : dr["HoraAbertura"].ToString();
                hor.HoraEncerramento = dr["HoraEncerramento"] == DBNull.Value ? "" : dr["HoraEncerramento"].ToString();
                hor.AtivoSN = dr["AtivoSN"] == DBNull.Value ? false : Convert.ToBoolean(dr["AtivoSN"].ToString());
                list.Add(hor);
            }
            return list;
        }
    }
}