﻿using BonnaWCF.Bll.Restaurante;
using BonnaWCF.Model.Restaurante;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace BonnaWCF
{
  
    public class Service1 : IService1
    {
        public List<RestauranteModel> RetornaRestaurantes()
        {
            RestauranteBLL x = new RestauranteBLL();
            return x.RetornaRestaurantes();
        }
        public List<RestauranteModel> RetornaRestaurantePorId(string Id)
        {
            RestauranteBLL x = new RestauranteBLL();
            return x.RetornaRestaurantePorId(Convert.ToInt32(Id));
        }
        public List<HorarioModel> RetornaHorarioRestaurante()
        {
            HorarioBLL x = new HorarioBLL();
            return x.RetornaHorarioRestaurantes();
        }
        public List<ContatoModel> RetornaContatoRestaurante()
        {
            ContatoBLL x = new ContatoBLL();
            return x.RetornaContatoRestaurante();
        }
    }
}
