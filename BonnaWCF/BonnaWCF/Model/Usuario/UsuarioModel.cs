﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace BonnaWCF.Model.Usuario
{
    [DataContract]
    public class UsuarioModel
    {
        [DataMember]
        public int IDUsuario { get; set; }
        [DataMember]
        public string Nome { get; set; }
        [DataMember]
        public string Sobrenome { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string Imagem { get; set; }
        [DataMember]
        public int CPF { get; set; }
        [DataMember]
        public string TipoUsuario { get; set; }
        [DataMember]
        public string Usuario { get; set; }
        [DataMember]
        public string Senha { get; set; }
        [DataMember]
        public string DataCadastro { get; set; }
        [DataMember]
        public string DataAtualizacao { get; set; }
    }
}