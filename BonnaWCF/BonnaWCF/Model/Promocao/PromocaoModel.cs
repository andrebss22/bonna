﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace BonnaWCF.Model.Promocao
{
    [DataContract]
    public class PromocaoModel
    {
        [DataMember]
        public int IDPromocao { get; set; }
        [DataMember]
        public int IDRestaurante { get; set; }
        [DataMember]
        public string Imagem { get; set; }
        [DataMember]
        public string Titulo { get; set; }
        [DataMember]
        public string Descricao { get; set; }
        [DataMember]
        public DateTime DataInicio { get; set; }
        [DataMember]
        public DateTime DataTermino { get; set; }
        [DataMember]
        public bool AtivoSN { get; set; }

    }
}