﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace BonnaWCF.Model.Restaurante
{
    [DataContract]
    public class ContatoModel
    {
        [DataMember]
        public int IDContato { get; set; }
        [DataMember]
        public int IDRestaurante { get; set; }
        [DataMember]
        public string Descricao { get; set; }
        [DataMember]
        public int Numero { get; set; }
        [DataMember]
        public bool FixoSN { get; set; }
    }
}