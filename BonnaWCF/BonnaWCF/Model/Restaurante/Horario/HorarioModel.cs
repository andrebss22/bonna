﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace BonnaWCF.Model.Restaurante
{
    [DataContract]
    public class HorarioModel
    {
        [DataMember]
        public int IDHorario { get; set; }
        [DataMember]
        public int IDRestaurante { get; set; }
        [DataMember]
        public string Dia { get; set; }
        [DataMember]
        public string HoraAbertura { get; set; }
        [DataMember]
        public string HoraEncerramento { get; set; }
        [DataMember]
        public bool AtivoSN { get; set; }
    }
}