﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace BonnaWCF.Model.Restaurante
{
    [DataContract]
    public class RestauranteModel
    {
        [DataMember]
        public int IDRestaurante { get; set; }

        [DataMember]
        public string Imagem { get; set; }

        [DataMember]
        public string Nome { get; set; }

        [DataMember]
        public string Descricao { get; set; }

        [DataMember]
        public int Cliques { get; set; }

        [DataMember]
        public bool RotacaoSN { get; set; }

        [DataMember]
        public bool PremiumSN { get; set; }

        [DataMember]
        public bool AtivoSN { get; set; }
    }
}