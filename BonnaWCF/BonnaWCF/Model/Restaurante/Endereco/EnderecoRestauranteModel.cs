﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace BonnaWCF.Model.Restaurante
{
    [DataContract]
    public class EnderecoRestauranteModel
    {
        [DataMember]
        public int IDEndereco { get; set; }
        [DataMember]
        public int IDRestaurante { get; set; }
        [DataMember]
        public string Descricao { get; set; }
        [DataMember]
        public string Logradouro { get; set; }
        [DataMember]
        public string Numero { get; set; }
        [DataMember]
        public int CEP { get; set; }
        [DataMember]
        public string Bairro { get; set; }
        [DataMember]
        public string Cidade { get; set; }
        [DataMember]
        public string Estado { get; set; }
        [DataMember]
        public bool MatrizSN { get; set; }
    }
}