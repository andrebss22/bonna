﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace BonnaWCF.Model.Tag
{
    [DataContract]
    public class TagModel
    {
        [DataMember]
        public int IDTag { get; set; }
        [DataMember]
        public int IDRestaurante { get; set; }
        [DataMember]
        public int Tag { get; set; }
        [DataMember]
        public int Peso { get; set; }
    }
}